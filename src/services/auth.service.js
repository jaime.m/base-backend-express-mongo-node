const {generateToken} = require('../helpers/jwt.helper')

let _userRepository=null

class AuthService {
    constructor({UserRepository}){
        _userRepository=UserRepository
    }
    async singin(user){

        const {username,password}=user
        const exist= await _userRepository.getUserByUsername(username)
        if(!exist){
            const error = new Error()
            error.status=404
            error.message='User not found'
            throw error
        }
        const realUser= exist.comparePasswords(password)
        if(!realUser){
            const error = new Error()
            error.status=400
            error.message='>Invalid password'
            throw error
        }
        const userToEncode ={
            username:realUser.username,
            id:realUser.id,
            role:realUser.role
        }   
        
        const token=generateToken(userToEncode)
        return {
            token,          
            user:exist,
        }
        
    }

    async singup(user){
        const {username,password}=user
        const exist= await _userRepository.getUserByUsername(username)
        if(exist){
            const error = new Error()
            error.status=404
            error.message='User Alrealdy exists'
            throw error
        }
        user.active=true
        user.role="USER_ROLE"
        return _userRepository.create(user)
    }
}

module.exports=AuthService