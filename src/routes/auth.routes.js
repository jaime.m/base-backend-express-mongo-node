const {Router} = require('express')
const {AuthMiddleware} = require('../middlewares')

module.exports= function({
    AuthController
}){
    const router=Router()
    router.post('/singin',AuthController.singin)
    router.post('/singup',AuthController.singup)
    return router
}
