const {Router}=require('express')
const {AuthMiddleware} = require('../middlewares')
const fileUpload = require('express-fileupload')


module.exports= function({
    UserController
}){
    const router = Router()
    router.use(fileUpload())
    router.get('/:userId',UserController.get)
    router.get('/',UserController.getAll)
    router.patch('/:_id',UserController.update)
    router.delete('/:userId',UserController.delete)

    return router
}